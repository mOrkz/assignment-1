package com.example.thomas.googlemaps;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.*;
import android.os.Bundle;
import android.provider.Settings;

import android.util.Log;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;


/*import com.google.android.gms.maps.model.MarkerOptions;*/







public class MainActivity extends Activity implements View.OnClickListener  {

       /* Local variables  */
        GoogleMap googleMap;
        double latitude;
        double longitude;
        double altitude;
        LatLng position;
        Button button1;

        /* https://developers.google.com/maps/documentation/android/start //Tutorial for adding map
        /* Initialises the mapview*/
        private void createMapView() {
            /**
             * Catch the null pointer exception that
             * may be thrown when initialising the map
             */

        try {
        if (null == googleMap) {
        googleMap = ((MapFragment) getFragmentManager().findFragmentById(
        R.id.mapView)).getMap();

        if(googleMap != null) {

        googleMap.setMyLocationEnabled(true);


        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        android.location.Location location = locationManager.getLastKnownLocation(provider);

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        position = new LatLng(latitude, longitude);
           /* https://developers.google.com/maps/documentation/android/location //Tutorial for finding current position
        /* googleMap.addMarker(new MarkerOptions().position(position).title("You are here!")); //add marker, position, title*/

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 14));
        // Returns a CameraUpdate that moves the camera viewpoint to a particular zoom level. http://developer.android.com/reference/com/google/android/gms/maps/CameraUpdateFactory.html
        }

        /**
        * If the map is still null after attempted initialisation,
        * show an error to the user
        */
        if (null == googleMap) {
        Toast.makeText(getApplicationContext(),
        "Error creating map", Toast.LENGTH_SHORT).show();
        }
        }
        } catch (NullPointerException exception) {
        Log.e("mapApp", exception.toString());
            }
        }

        /* Check if the gps is enabled http://android-er.blogspot.no/2010/10/check-and-prompt-user-to-enable-gps.html //Checks if the gps is on */
        private void CheckEnableGPS(){
        String provider = Settings.Secure.getString(getContentResolver(),
        Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.equals("")){
        //Providers Enabled
        Toast.makeText(MainActivity.this, "Providers Enabled: " + provider,
        Toast.LENGTH_LONG).show();
        }else{
        Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
        startActivity(intent);
            }

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createMapView(); //creates the mapview
        CheckEnableGPS(); //check what provider that is enabled
        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(this); //onclick --> location.java

        }

        private void button1Click() /*https://www.youtube.com/watch?v=fFOPzmeknNo //Make a new activity + button */
        {

            Intent intent = new Intent(this, Location.class);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            intent.putExtra("altitude", altitude);


             /* http://developer.android.com/reference/android/content/Intent.html */

            startActivity(intent);
       }

         @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.button1:
                    button1Click();
                    break;
            }

        }
    }



