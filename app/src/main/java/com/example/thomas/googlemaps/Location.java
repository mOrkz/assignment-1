package com.example.thomas.googlemaps;



import android.app.Activity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.TextView;

import java.io.IOException;
/*import java.text.DecimalFormat; */
import java.util.List;
import java.util.Locale;


public class Location extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        double latitude = getIntent().getExtras().getDouble("latitude");
        double longitude = getIntent().getExtras().getDouble("longitude");
        double altitude = getIntent().getExtras().getDouble("altitude");

        TextView myTextView = (TextView) findViewById(R.id.textView1);myTextView.setText("Latitude: " + latitude);
        TextView textView2 = (TextView) findViewById(R.id.textView2);textView2.setText("Longitude: " + longitude);
        TextView myAddress = (TextView)findViewById(R.id.myaddress);
        TextView textView3 = (TextView)findViewById(R.id.altitude);textView3.setText("Currently " + altitude +" meters above sea level");



        //Use reverse geocoding, so I can retrieve address.  http://developer.android.com/training/location/display-address.html //Tutorial for address, city etc.
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        try {
        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

        if(addresses != null) {
        Address returnedAddress = addresses.get(0);
        StringBuilder strReturnedAddress = new StringBuilder("You are now at:\n");
        for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n"); /* DecimalFormat dF = new DecimalFormat("#,#"); */
        }
        myAddress.setText(strReturnedAddress.toString());
            }
        else{
        myAddress.setText("No Address returned!");
            }
        } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        myAddress.setText("Cannot get Address!");
        }

    }



    }





